Pay With Amazon Integration.

This module will display a "Pay With Amazon" button on the checkout page which
will help websites increase the conversion or improve sales by utilizing Amazon
payments and addresses stored by user in Amazon.

To configure this module, 

1. Download the module.
2. Download the library from https://github.com/nkgokul/paywithamazonfordrupal
   to sites/all/libraries folder.
3. Enable the module. (Make sure you have libraries module enabled)
4. Go to admin/commerce/config/payment-methods/pay-with-amazon.
5. Enter your Amazon Seller Credentials.

Once you enter your credentials add a product to your cart and go to checkout
page to see the "Pay with Amazon" button.

Module was developed for
<a href="https://www.aziteez.com">Aziteez</a>.
