<?php
  
/**
 * @file
 *   drush integration for Pay with Amazon.
 */

define('AMAZON_LIB_DOWNLOAD_URI', 'https://github.com/nkgokul/paywithamazonfordrupal/archive/master.zip');

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * See `drush topic docs-commands` for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function pay_with_amazon_drush_command() {
  $items = array();

  // the key in the $items array is the name of the command.
  $items['pay-with-amazon-lib'] = array(
    'callback' => 'drush_pay_with_amazon_lib',
    'description' => dt("Downloads the PayWith Amazon Library."),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap.
    'aliases' => array('pwal'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function pay_with_amazon_drush_help($section) {
  switch ($section) {
    case 'drush:pay-with-amazon-lib':
      return dt("Downloads the PayWithAmazon Library, default location is sites/all/libraries.");
  }
}


/**
 * Command to download the pay with Amazon library.
 */
function drush_pay_with_amazon_lib() {
  if (!drush_shell_exec('type unzip')) {
    return drush_set_error(dt('Missing dependency: unzip. Install it before using this command.'));
  }

  $args = func_get_args();
  if ($args[0]) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  $filename = basename(AMAZON_LIB_DOWNLOAD_URI);
  $dirname = basename(AMAZON_LIB_DOWNLOAD_URI, '.zip');

  // Remove any existing Pay with Amazon directory
  if (is_dir($dirname)) {
    drush_log(dt('A existing Pay with Amazon was found and overwritten at @path', array('@path' => $path)), 'notice');
  }
  // Remove any existing Pay with Amazon zip archive
  if (is_file($filename)) {
    drush_op('unlink', $filename);
  }

  // Download the zip archive
  if (!drush_shell_exec('wget ' . AMAZON_LIB_DOWNLOAD_URI)) {
    drush_shell_exec('curl -O ' . AMAZON_LIB_DOWNLOAD_URI);
  }

  if (is_file($filename)) {
    // Decompress the zip archive
    drush_shell_exec('unzip -qq -o ' . $filename);
    // Rename directory name appropriately.
    drush_shell_exec('mv paywithamazonfordrupal-master pay_with_amazon');
    // Remove the zip archive
    drush_op('unlink', $filename);
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);

  if (is_dir($path . '/' . $dirname)) {
    drush_log(dt('Drush was unable to download the Pay with Amzon library to @path', array('@path' => $path)), 'error');
  }
  else {
    drush_log(dt('Pay with Amazon library has been downloaded to @path', array('@path' => $path)), 'success');
  }
}
?>